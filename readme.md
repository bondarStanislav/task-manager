# Task Manager


Powered by [Stan Bondar](https://www.linkedin.com/in/stanislav-bondar/)

## Installation
```sh
git clone https://gitlab.com/bondarStanislav/task-manager.git
```
```sh
npm i
```
## Development
```sh
npm run start
```

## Building for source

```sh
npm run start
```
```sh
serve -s build
```