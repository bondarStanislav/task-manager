import * as React from 'react'
import {Header} from './components/Header'
import {TaskList} from './components/TaskList'
import {Footer} from './components/Footer'
import {useState} from 'react'
import Grid from '@mui/material/Grid'

type Filter = 'all' | 'done'

export default function App() {
	const [searchTerm, setSearchTerm] = useState('')
	const [filter, setFilter] = useState<Filter>('all')
	const handleSearch = (value: string) => {
		setSearchTerm(value)
	}

	const clearFilter = () => {
		setFilter('all')
		setSearchTerm('')
	}

	const handleDone = () => {
		setFilter('done')
	}

	return (
		<Grid
			sx={{
				p: 2,
				margin: 'auto',
				maxWidth: 500,
				flexGrow: 1,
				verticalAlign: 'middle',
			}}
		>
			<Header
				handleSearch={handleSearch}
				handleAll={clearFilter}
				handleDone={handleDone}
			/>

			<TaskList
				searchTerm={searchTerm}
				filter={filter}
			/>

			<Footer  />
		</Grid>
	)
}
