import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import theme from '../../theme'
import React  from 'react'
export const labelSet = {
	'done' : {
		id: 0,
		icon: <CheckCircleOutlineIcon sx={{
			color: theme.color.main,
			transition: 'color .25s',
		}}/>,
		label: 'Done',
		route: 'route',
	},
	'delete' : {
		id: 1,
		icon: <DeleteForeverIcon />,
		label: 'Delete',
		route: 'route',
	},

}