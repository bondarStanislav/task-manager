import {styled} from '@mui/material/styles'
import TextField, {TextFieldProps} from '@mui/material/TextField'
import theme from '../../theme'
import React from 'react'

const StyledTextField = styled(TextField)(() => ({
	width: '70%',
	borderColor: theme.color.lightGray,
	outlineColor: theme.color.main,
	'& .MuiOutlinedInput-root': {
		height: theme.size.height
	},
	'& .MuiOutlinedInput-root.Mui-focused': {
		'& > fieldset': {
			borderColor: theme.color.main
		},
	},
	'& label': {
		top: '-6px',
	},
	'& label.Mui-focused': {
		color: theme.color.main,
	},
	'& .MuiOutlinedInput-root.Mui-error': {
		'& > fieldset': {
			borderColor: theme.color.danger + '!important'
		}
	},
	fieldset: {
		borderColor: theme.color.light,
		'& label.Mui-focused': {
			borderColor: theme.color.main,
		},
	}
}))

const ToDoTextField = React.forwardRef((props: TextFieldProps, ref: React.ForwardedRef<HTMLInputElement>) => {
	return <StyledTextField {...props} ref={ref} />
})

ToDoTextField.displayName = 'ToDoTextField'
export default ToDoTextField
