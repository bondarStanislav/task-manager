import Checkbox from '@mui/material/Checkbox'
import {labelSet} from './consts/labelSet'
import {Dialog, DialogActions, DialogTitle, Stack} from '@mui/material'
import {DeleteButton} from './Buttons'
import Typography from '@mui/material/Typography'
import theme from '../theme'
import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {deleteTask, toggleTask} from '../reducers/taskSlice'
import Button from '@mui/material/Button'

interface TaskItemProps {
    id: string,
    title: string,
    completed: boolean,
}
export const TaskItem: React.FC<TaskItemProps> = ({ id, title, completed }) =>  {
	const [open, setOpen] = useState(false)
	const dispatch = useDispatch()

	const handleClickOpen = (e: React.MouseEvent<HTMLButtonElement>) => {
		e.stopPropagation()
		setOpen(true)
	}

	const handleClose = () => {
		setOpen(false)
	}

	const handleDelete = () => {
		dispatch(deleteTask(id))
		setOpen(false)
	}

	return (
		<Stack direction="row" spacing={2} alignItems={'center'}
			sx={{'&:hover': {backgroundColor: theme.color.lightGray, cursor: 'pointer'}}}         >
			<Checkbox sx={{ color: theme.color.darkGray,'&.Mui-checked': {color: theme.color.main} }}
				checked={completed}
				onClick={(e) => e.stopPropagation()}
				onChange={() => dispatch(toggleTask(id))}
			/>

			<Typography variant={'body1'} className={'completed'}
				sx={{
					maxWidth: 310,
					textDecoration: completed ? 'line-through' :'none',
				}}
			>
				{title}
			</Typography>

			<DeleteButton
				onClick={handleClickOpen}
				aria-label="delete"
			>
				{labelSet.delete.icon}
			</DeleteButton>

			<Dialog
				open={open}
				onClose={handleClose}
			>
				<DialogTitle>{'Are you sure?'}</DialogTitle>
				<DialogActions
					sx={{
						justifyContent: 'center'
					}}
				>
					<Button
						onClick={handleClose}
						autoFocus
						sx={{
							color: theme.color.main
						}}
					>
						No
					</Button>
					<Button
						onClick={handleDelete}
						sx={{
							color: theme.color.danger
						}}
					>
						Yes
					</Button>
				</DialogActions>
			</Dialog>

		</Stack>
	)
}