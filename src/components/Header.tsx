import {labelSet} from './consts/labelSet'
import {ButtonFilled, ButtonOutlined} from './Buttons'
import React, {FC} from 'react'
import {Stack} from '@mui/material'
import ToDoTextField from './TextField'
interface HeaderProps {
    handleSearch: (value: string) => void
    handleAll: () => void,
    handleDone: () => void
}

export const Header: FC<HeaderProps> = ({ handleSearch, handleAll, handleDone }) => {

	return (
		<header>
			<Stack
				direction={'row'}
				spacing={{ xs: 1, sm: 2 }}
				justifyContent={'space-between'}
				alignItems={'start'}
			>
				<ToDoTextField
					margin="normal"
					id="search"
					label="Search by text..."
					name="email"
					sx={{
						width: '60%',
						maxHeight: '100%',
						margin: 0,
						'& .MuiInputBase-root': {
							margin: 0,
						}
					}}
					onChange={(e) => handleSearch(e.target.value)}
				/>

				<ButtonFilled
					variant="contained"
					onClick={handleAll}
				>
                All
				</ButtonFilled>

				<ButtonOutlined
					variant="outlined"
					startIcon={labelSet.done.icon}
					onClick={handleDone}
				>
					{labelSet.done.label}
				</ButtonOutlined>
			</Stack>
		</header>
	)
}