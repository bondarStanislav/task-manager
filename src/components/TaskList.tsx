import {Stack} from '@mui/material'
import {TaskItem} from './TaskItem'
import {useSelector} from 'react-redux'
import {selectTasks} from '../reducers/taskSlice'
import React from 'react'

interface TaskListProps {
    searchTerm: string,
    filter: 'all' | 'done'
}

export const TaskList: React.FC<TaskListProps> = ({ searchTerm, filter }) => {
	const tasks = useSelector(selectTasks)

	const filteredtasks = tasks
		.filter((task) =>
			searchTerm
				? task.title.toLowerCase().startsWith(searchTerm.toLowerCase())
				: true
		)
		.filter((task) =>
			filter === 'done'
				? task.completed
				: true
		)
		.sort((a, b) => (a.completed === b.completed ? 0 : a.completed ? 1 : -1))

	return (
		<Stack
			direction="column"
			spacing={1}
			sx={{
				marginTop: 3,
				marginBottom: 3,
			}}>
			{filteredtasks.map((task) => (
				<TaskItem
					key={task.id}
					id={task.id}
					title={task.title}
					completed={task.completed}
				/>
			))}
		</Stack>
	)
}