import {styled} from '@mui/material/styles'
import Button from '@mui/material/Button'
import {alpha, IconButton} from '@mui/material'

export const ButtonBordered = styled(Button)(({ theme }) => ({
	borderColor: theme.color.main,
	backgroundColor: alpha(theme.color.main, 0),
	color: theme.color.main,
	textTransform: 'none',
	minWidth: '120px',
	height: theme.size.height,
	'&.MuiButton-root:hover': {
		borderColor: theme.color.main,
		backgroundColor: theme.color.main,
		color: theme.color.white,
	},
})) as typeof Button

export const ButtonOutlined = styled(Button)(({ theme }) => ({
	borderColor: theme.color.light,
	backgroundColor: alpha(theme.color.main, 0),
	color: theme.color.black,
	textTransform: 'none',
	minWidth: '87px',
	height: theme.size.height,
	'&.MuiButton-root:hover': {
		borderColor: theme.color.main,
		backgroundColor: theme.color.main,
		color: theme.color.white,
		'& .MuiSvgIcon-root' : {
			color: theme.color.white,
			transition: 'color .25s',
		}
	},
})) as typeof Button

export const ButtonFilled = styled(Button)(({ theme }) => ({
	borderColor: theme.color.main,
	backgroundColor: theme.color.main,
	color: theme.color.white,
	textTransform: 'none',
	minWidth: '87px',
	height: theme.size.height,
	boxShadow: 'none',
	'&.MuiButton-root:hover': {
		borderColor: theme.color.main,
		backgroundColor: theme.color.main,
		color: theme.color.white,
	},
}))

export const DeleteButton = styled(IconButton)(({ theme }) => ({
	'&.MuiIconButton-root': {
		marginLeft: 'auto',
		color: theme.color.darkGray,
	},
	'&:hover': {
		color: theme.color.danger,
		backgroundColor: 'transparent',
	},
})) as typeof Button