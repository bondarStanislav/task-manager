import {ButtonBordered} from './Buttons'
import ToDoTextField from './TextField'
import React, {FC} from 'react'
import {useDispatch} from 'react-redux'
import { useForm, Controller } from 'react-hook-form'
import {addTask} from '../reducers/taskSlice'
import {Stack} from '@mui/material'
import theme from '../theme'

interface FormInputs {
    title: string
}

export const Footer: FC = () => {
	const dispatch = useDispatch()
	const { control, handleSubmit, reset, formState: { errors } } = useForm<FormInputs>({
		mode: 'all',
	})
	const onSubmit = ({ title }: FormInputs) => {
		dispatch(addTask(title))
		reset()
	}
	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Stack
				direction={'row'}
				spacing={{ xs: 1, sm: 2 }}
				justifyContent={'space-between'}
				alignItems={'start'}
			>
				<Controller
					name="title"
					control={control}
					rules={{ required: true, minLength: 2 }}
					render={({ field }) =>
						<ToDoTextField
							{...field}
							error={!!errors.title && field.value.length > 0}
							helperText={errors.title && 'Todo title must be 2 characters or more'}
							label="Your task"
							placeholder='Write your checklist text here'
							variant="outlined"
							value={field.value || ''}
							InputLabelProps={{ style: { color: (errors.title && field.value.length > 0) ? theme.color.danger : undefined } }}
						/>
					}
				/>
				<ButtonBordered variant="outlined" type="submit">
					Add
				</ButtonBordered>
			</Stack>
		</form>
	)
}