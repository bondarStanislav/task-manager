import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'

interface Task {
    id: string
    title: string
    completed: boolean
}

export interface TaskState {
    tasks: Task[]
    selectedTaskIds: string[]
}

const initialState: TaskState = {
	tasks: [],
	selectedTaskIds: []
}

export const taskSlice = createSlice({
	name: 'tasks',
	initialState,
	reducers: {
		addTask: (state, action: PayloadAction<string>) => {
			state.tasks.push({ id: Date.now().toString(), title: action.payload, completed: false })
		},
		toggleTask: (state, action: PayloadAction<string>) => {
			const task = state.tasks.find(task => task.id === action.payload)
			if (task) {
				task.completed = !task.completed
			}
		},
		deleteTask: (state, action: PayloadAction<string>) => {
			state.tasks = state.tasks.filter(task => task.id !== action.payload)
			state.selectedTaskIds = state.selectedTaskIds.filter(id => id !== action.payload)
		},
		markTaskAsDone: (state) => {
			state.tasks = state.tasks.map(task => state.selectedTaskIds.includes(task.id) ? { ...task, completed: true } : task)
			state.selectedTaskIds = [] // Reset selectedTodoIds after marking as done
		}
	}
})

export const { addTask, toggleTask, deleteTask, markTaskAsDone } = taskSlice.actions

export const selectTasks = (state: RootState): Task[] => state.tasks.tasks
export default taskSlice.reducer
