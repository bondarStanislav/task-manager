import { combineReducers } from 'redux'
import taskSlice from './taskSlice'

const rootReducer = combineReducers({
	tasks: taskSlice,
})

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer
