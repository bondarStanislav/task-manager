import {createTheme} from '@mui/material/styles'
import {green} from '@mui/material/colors'

declare module '@mui/material/styles' {
    interface Theme {
        color: {
            white: string;
            main: string;
            light: string;
            black: string;
            danger: string;
            lightGray: string;
            darkGray: string;
        },
        size: {
            height: string;
        };
    }
    interface ThemeOptions {
        color?: {
            white?: string;
            main?: string;
            light?: string;
            black?: string;
            danger?: string;
            lightGray?: string;
            darkGray?: string;
        },
        size?: {
            height?: string;
        };
    }
}

const theme = createTheme({
	color: {
		white: '#FFFFFF',
		black: '#000000',
		main: green[500],
		light: '#F2EDED',
		danger: '#F33A3D',
		lightGray: '#F3F3F3',
		darkGray: '#C4C4C4',
	},
	size: {
		height: '43px',
	},

})

export default theme

